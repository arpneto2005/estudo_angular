import { Component, OnInit } from '@angular/core';

import { Frases } from '../shared/frases.model';
import { FRASES } from './frase-mock';

@Component({
  selector: 'app-painel',
  templateUrl: './painel.component.html',
  styleUrls: ['./painel.component.css']
})
export class PainelComponent implements OnInit {

  public frase: Frases[] = FRASES;
  public instrucao: String = 'Traduza a frase:';
  public resposta: String = '';
  public rodada: number = 0;
  public rodadaFrase: Frases ;

  public progresso: number = 0

  public tentativas: number = 3;

  constructor() { 
    this.atualizarRodada();
  }

  ngOnInit() {
  }

  public atualizaResposta(resposta: Event): void{
    this.resposta = (<HTMLInputElement>resposta.target).value;
    //console.log((<HTMLInputElement>resposta.target).value );
  }

  public verificaResposta(): void{
    console.log(this.tentativas);
    if(this.rodadaFrase.frasePtBr == this.resposta){
      alert('Tradução correta');
      //Mudando de Rodada
      this.rodada++;
      //Alterando a barra de progresso
      this.progresso = this.progresso + (100 / this.frase.length);
      //Atualização da Frase
      this.atualizarRodada();
    }else{
      this.tentativas--;

      if(this.tentativas === -1){
        alert('Você perdeu todas as Tentativas');
      }
    }
    //console.log('Verifica Resposta: ' + this.resposta)
    console.log(this.tentativas);
    console.log(this.rodadaFrase);
  }

  public atualizarRodada(): void{
     //Atualização de Frase
     this.rodadaFrase = this.frase[this.rodada];
     //Limpando o campo textarea
     this.resposta = '';
  }
}
