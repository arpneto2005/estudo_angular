import { Component, OnInit, Input } from '@angular/core';
import { Coracao } from '../shared/coracao.model';

@Component({
  selector: 'app-tentativas',
  templateUrl: './tentativas.component.html',
  styleUrls: ['./tentativas.component.css']
})
export class TentativasComponent implements OnInit {
  /*
  public coracaoVazio: String = 'src/assets/coracao_vazio.png';
  public coracaoCheio: String = 'src/assets/coracao_cheio.png';
  */
  @Input() public tentativas: Number;

  public coracoes: Coracao[] = [
    new Coracao(true), new Coracao(true), new Coracao(true) 
  ];

  constructor() { 
    //console.log(this.coracoes);
    
  }

  ngOnInit() {
    console.log('Teste de variavel recebida -tentativas-', this.tentativas);
  }

}
