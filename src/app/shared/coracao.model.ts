export class Coracao{
    constructor(
        public cheio: boolean,
        public coracaoCheio: String = 'src/assets/coracao_cheio.png',
        public coracaoVazio: String = 'src/assets/coracao_vazio.png'
    ){}

    public exibeCoracao(): String{
        if(this.cheio){
            return this.coracaoCheio;
        }else{
            return this.coracaoVazio;
        }
    }
}