import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrograssoComponent } from './prograsso.component';

describe('PrograssoComponent', () => {
  let component: PrograssoComponent;
  let fixture: ComponentFixture<PrograssoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrograssoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrograssoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
