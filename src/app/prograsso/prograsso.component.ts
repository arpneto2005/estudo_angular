import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-prograsso',
  templateUrl: './prograsso.component.html',
  styleUrls: ['./prograsso.component.css']
})
export class PrograssoComponent implements OnInit {

  @Input()  public progresso: number = 0;

  constructor() { }

  ngOnInit() {
  }

}
